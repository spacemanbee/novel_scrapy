# -*- coding: utf-8 -*-
from scrapy import Spider
from selenium import webdriver
from scrapy.selector import Selector
import time
from scrapy.http import Request
# mafan是繁簡字體轉換器
import mafan
import DbAccess


class QiDianSpider(Spider):
    name = 'qidian'
    # allowed_domains = ['www.qidian.com']
    start_urls = [
        'https://www.qidian.com/rank/hotsales?chn=21&page=1',   #玄幻
        'https://www.qidian.com/rank/hotsales?chn=1&page=1',    #奇幻
        'https://www.qidian.com/rank/hotsales?chn=2&page=1',    #武侠
        'https://www.qidian.com/rank/hotsales?chn=22&page=1',   #仙侠
        'https://www.qidian.com/rank/hotsales?chn=4&page=1',    #都市
        'https://www.qidian.com/rank/hotsales?chn=15&page=1',   #现实
        'https://www.qidian.com/rank/hotsales?chn=6&page=1',    #军事
        'https://www.qidian.com/rank/hotsales?chn=5&page=1',    #历史
        'https://www.qidian.com/rank/hotsales?chn=7&page=1',    #游戏
        'https://www.qidian.com/rank/hotsales?chn=8&page=1',    #体育
        'https://www.qidian.com/rank/hotsales?chn=9&page=1',    #科幻
        'https://www.qidian.com/rank/hotsales?chn=10&page=1',   #灵异
        'https://www.qidian.com/rank/hotsales?chn=12&page=1'    #二次元
    ]

    def __init__(self):
        self.__CONTENT_WORD_NUMBER_LIMIT = 50
        self.__WAIT_AJAX_TIME = 6
        self.driver = webdriver.Chrome('chromedriver.exe')
        self.TYPENUMBER = {'奇幻': 1, '武俠': 2, '玄幻': 3, '仙俠': 4, '女生網': 5, '都市': 6, '遊戲': 7,
                           '靈異': 8, '軍事': 9, '二次元': 10, '科幻': 11, '體育': 12, '現實': 13, '歷史': 14, '其他': 15}

    def __del__(self):
        self.driver.quit()

    def parse(self, response):
        self.driver.get(response.url)
        sel = Selector(text=self.driver.page_source)
        # 找書籍詳情按鈕中的href
        bookHrefXpath='//*[@id="rank-view-list"]/div/ul/li/div[@class="book-right-info"]/p/a[@class="red-btn"]/@href'
        books = sel.xpath(bookHrefXpath).extract()
        # 測試專用
        # url = 'https:' + books[0]
        for book in books:
            url = 'https:' + book
            self.parse_book(url)
            # yield Request(url, callback=self.parse_book)
        return

    def parse_book(self, url):
        self.driver.get(url)
        # 先等待網頁連同javascripct部分都loading 好
        time.sleep(self.__WAIT_AJAX_TIME)
        sel = Selector(text=self.driver.page_source)
        self.make_item_novel(sel)

    # 其實是把幾篇書本的評論拿來做成一篇post，不是真的novel
    def make_item_novel(self, htmlcontent):
        # 一篇novel post格式暫訂如下(仿照ptt格式)，請對照novel資料表的格式
        # source: 2 (起點網為2)
        # title：[<書名>][<書本作者>]<第一句話到空白或標點符號為止>
        # user：<評論作者名稱>   (起點網的user沒有user_id)
        # user_id: 0
        # category_id: <類型>   #很多類型的話選第一個有在14個分類裡面的
        # subcategory_id: 0
        # update_time: <po文時間>
        # vote: 0
        # content: <超過100字的評論>
        bookTitle = htmlcontent.xpath('/html/body/div[2]/div[6]/div[1]/div[2]/h1/em/text()').extract()
        bookAuthor = htmlcontent.xpath('/html/body/div[2]/div[6]/div[1]/div[2]/h1/span/a/text()').extract()
        bookType = htmlcontent.xpath('/html/body/div[2]/div[6]/div[1]/div[2]/p[1]/a/text()').extract()
        # bookInfo = htmlcontent.xpath('/html/body/div[2]/div[6]/div[4]/div[1]/div[1]/div[1]/p/text()').extract()
        # bookVote = htmlcontent.xpath('//p[@class="num"]/i/text()').extract()
        # bookDiscuss = htmlcontent.xpath('//div[@class="discuss-info"]/h5/a/text()').extract()
        # 接下來去parse評論   產生item
        postContent = self.make_item_friend_comment(htmlcontent)
        if not postContent:
            return
        title = '[' + mafan.tradify(bookTitle[0]) + '][' + mafan.tradify(bookAuthor[0]) + ']'
        category = self.determine_book_type(bookType)
        article = {'title': title, 'type': category, 'content': postContent}
        db = DbAccess.DbAccess()
        db.add_novel_post_to_db(article)

    def make_item_friend_comment(self, htmlcontent):
        comments = htmlcontent.xpath('//div[@class="comment-info"]').extract()
        # print("ccccccccccc"+str(len(comments)))
        commentItem = []
        for cm in comments:
            # print(cm)
            cmsel = Selector(text=cm)
            content = cmsel.xpath('//p[@class="extend"]/text()').extract()
            if not content:
                continue
            contentLen = len(content[0])
            # print(contentLen)
            if contentLen < self.__CONTENT_WORD_NUMBER_LIMIT:
                continue
            author = cmsel.xpath('//a[@class="blue"]/text()').extract()
            like = cmsel.xpath('//a[1]/b/text()').extract()
            postTime = cmsel.xpath('//div[@class="comment-info"]/p[2]/text()').extract()
            if not author or not like or not postTime:
                continue
            comment = [postTime[0], mafan.tradify(author[0]), mafan.tradify(content[0]), like[0]]
            # print(comment)
            commentItem.append(comment)
        # print(commentItem)
        return commentItem

    # 這裡判斷category，把文字換成代號，會回傳第一個檢查到的代號
    def determine_book_type(self, booktypes):
        for bt in booktypes:
            traditionbt = mafan.tradify(bt)
            if traditionbt in self.TYPENUMBER:
                return self.TYPENUMBER[traditionbt]
        return 0
