# -*- coding: utf-8 -*-
# import scrapy
from scrapy import Spider
from selenium import webdriver
from scrapy.selector import Selector
from scrapy.http import Request


class BooksSpider(Spider):
    name = 'books3'
    allowed_domains = ['books.toscrape.com']

    def __init__(self):
        self.driver = webdriver.Chrome('chromedriver.exe')

    def start_requests(self):
        self.driver.get('http://books.toscrape.com')
        sel = Selector(text=self.driver.page_source)
        books = sel.xpath('//h3/a/@href').extract()
        for book in books:
            url = 'http://books.toscrape.com/' + book
            yield Request(url, callback=self.parse_book)

    def parse_book(self, response):
        sel = Selector(response)
        # item = scrapy.Item()
        upc = sel.xpath('//*[@id="content_inner"]/article/table/tr[1]/td/text()').extract()
        print("----"+str(upc))
        pass
