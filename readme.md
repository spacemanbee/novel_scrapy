#執行本爬蟲環境(python 3.6)需要安裝以下套件
	1. Scrapy
	2. VC++ 2014 build tools (這個不是python套件，Windows系統需要先裝這個才能裝Scrapy)
	3. selenium
	4. selenium driver (這不是python套件，是Selenium所需的額外程式，目前code是用Chrome，請根據作業系統版本下載，下載後請放到跟本文件相同的資料夾下)
	4. mafan
	5. mysqlclient
	6. pypiwin32 (Windows系統專用)
	
#如何測試爬蟲功能
	1. 打開具有以上環境的terminal，並切換資料夾至本文件所在資料夾
	2. 輸入 scrapy crawl qidian 即可(目前qidian為起點中文網spider的代稱)

#如何定時執行爬蟲
    1. 請確定venv資料夾在webScan.py的母資料夾裡面(不是用虛擬環境的請註解掉webScan.py的line18和19)
    2. 執行timetrigger.py即可