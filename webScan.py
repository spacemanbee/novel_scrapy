# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import subprocess
from subprocess import CalledProcessError
import os


class StartScrapy:
    def __init__(self):
        self.cmdActivateScrapy = 'scrapy crawl qidian'

    def start_scan(self):
        my_env = os.environ.copy()
        dirpath = os.path.abspath(os.pardir)+'\\venv;'
        my_env["PATH"] = dirpath + my_env["PATH"]
        try:
            subprocess.Popen(self.cmdActivateScrapy, env=my_env)
        except CalledProcessError as e:
            print(e.output)
        else:
            print("執行成功")


# s = StartScrapy()
# s.start_scan()
