# -*- coding: utf-8 -*-
"""
Created on Sat May 13 19:20:50 2017

@author: Spaceman
"""
import random
import webScan
#from datetime import datetime
#now = datetime.now()
#startTime = datetime.strptime("20:30:43")
#delta = random.gammavariate(alpha=4.7481, beta=1.1346)
#deltams = delta*1000*60now.hour
#from datetime import timedelta
#run_at = now + timedelta(milliseconds=deltams)
#delay = (run_at - now).total_seconds()

from apscheduler.schedulers.blocking import BlockingScheduler
import apscheduler.events as events
#from apscheduler.triggers.cron import CronTrigger

from datetime import datetime
from datetime import timedelta
# adjustedWorkingDayList = ['2017-06-03','2017-09-30']
# holidayList = ['2018-01-01','2018-02-15','2018-02-16','2018-02-19','2018-02-20']
scanTime = "05:33:00"
# signOutTime= "19:07:43"

scheduler = BlockingScheduler(misfire_grace_time=900)


# def need_work_date(dateInput):
#     if dateInput.strftime("%Y-%m-%d") in holidayList:
#         return False
#     if (dateInput.isoweekday() > 5) and (dateInput.strftime("%Y-%m-%d") not in adjustedWorkingDayList):
#         return False
#     return True

# def next_work_day_after_date(dateInput):
#     temp = dateInput + timedelta(days=1)
#     while not need_work_date(temp):
#         temp = temp + timedelta(days=1)
#     return temp

def job():
    s = webScan.StartScrapy()
    s.start_scan()
    # s.start_sign_in()


def trigger_time():
    nowTime = datetime.now()
    scrapyTimeWithDate = datetime.strptime(nowTime.strftime("%Y-%m-%d")+" "+scanTime, "%Y-%m-%d %H:%M:%S")
    # signOutTimeWithDate = datetime.strptime(nowTime.strftime("%Y-%m-%d")+" "+signOutTime,"%Y-%m-%d %H:%M:%S")
    scheduleTime = scrapyTimeWithDate
    if nowTime >= scheduleTime:
        # temp = scheduleTime + timedelta(minutes=5)
        scheduleTime = scheduleTime + timedelta(days=1)
        # temp = nowTime + timedelta(minutes=4)
        # scheduleTime = datetime.strptime(temp.strftime("%Y-%m-%d")+" "+scanTime, "%Y-%m-%d %H:%M:%S")
        # scheduleTime = temp

    # 先弄個gamma隨機變數
#    delta = random.gammavariate(alpha=1, beta=1)
#     delta = random.gammavariate(alpha=5.7481, beta=3.846)
#     deltams = delta*1000*60
#     runAt = scheduleTime + timedelta(milliseconds=deltams)
#    runAt = datetime.now() + timedelta(seconds=3)
    print("下次預期刷卡時間為"+scheduleTime.strftime("%Y-%m-%d %H:%M:%S"))
    return scheduleTime


def job_listener(event):
    if event.code == events.EVENT_JOB_MISSED:
        print("events.EVENT_JOB_MISSED")
        scheduler.remove_all_jobs()
        job()
    elif event.exception:
        print("event.exception")
        scheduler.remove_all_jobs()
        job()
    scheduler.add_job(job, 'date', run_date=trigger_time())
        
scheduler.add_listener(job_listener,
                       events.EVENT_JOB_EXECUTED |
                       events.EVENT_JOB_MISSED |
                       events.EVENT_JOB_ERROR)
scheduler.add_job(job, 'date', run_date=trigger_time())
scheduler.start()
# print("test 01")
