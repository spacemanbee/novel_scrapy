# -*- coding: utf-8 -*-
"""
-------------------------
Update
-------------------------
20170815-spaceman:
1. 因應Python版本不同導致資料庫存取方式有差異，新增對Python 3.6及舊版的支援

20170813-spaceman:
1. 新增update_all_restaurant_picture_from_ref()，根據google ref抓圖存在電腦裡並更新url至資料庫
2. 新增truncate_table()，輸入table名稱，可清除該table所有內容但保留其結構(adminer或php不能用時可以用這個)

Created on Tue Mar 21 11:34:08 2017
這裡負責所有資料庫的存取
@author: Spaceman
"""
import sys
# import GooglePic
import re
"""
注意python3.6版後得改用MySQLdb的packet, 套件名稱為mysqlclient，請自行下指令安裝
之前的版本好像能用mysql-connector(改用3.6後，無法正常安裝此套件)
"""
if sys.version_info >= (3, 6):
    import MySQLdb as mariadb
else:
    import mysql.connector as mariadb


class DbAccess:
    def __init__(self):
        self.__conn = None
        self.__cursor = None
        self.__host = '140.114.79.167'
        self.__user = 'Bee'
        self.__pw = 'huang'
        self.__dbName = 'novel'

    def __connect_db(self):
        if sys.version_info >= (3, 6):
            self.__conn = mariadb.connect(host=self.__host,
                                          user=self.__user,
                                          passwd=self.__pw,
                                          db=self.__dbName,
                                          use_unicode=True,
                                          charset="utf8")
        else:
            config = {
                'user': 'Bee',
                'password': 'huang',
                'host': '140.114.79.167',
                'database': 'novel',
                'raise_on_warnings': True,
            }
            self.__conn = mariadb.connect(**config)
        self.__cursor = self.__conn.cursor()
        if self.__cursor is not None:
            return True;
        return False

    def __check_connect_db(self):
        if self.__conn is None or self.__cursor is None:
            return self.__connect_db()
        else:
            return True

    def __del__(self):
        if self.__conn:
            self.__conn.close()
        else:
            print ("DbAccess del none")

    def add_novel_post_to_db(self, post):
        if self.__check_connect_db():
            # 先檢查有沒有重複的  這裡先用user和time做檢查就好
            for cm in post['content']:
                sql = "SELECT COUNT(*) FROM `novel` WHERE `user`='" + cm[1] + "' AND `update_time`='" + cm[0] + "'"
                # print(sql)
                self.__cursor.execute(sql)
                num = self.__cursor.fetchone()
                # print(num)
                if num[0] > 0:
                    # print("post重複")
                    continue
                try:
                    lastTitle = re.split("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，：；。？、~@#￥%……&*（）]+", cm[2])
                    # print("@@@1  " + post['title'] + lastTitle[0])
                    sql = "INSERT INTO `novel` (`source`, `title`, `user`, `category_id`, `subcategory_id`, `update_time`, `vote`, `content`, `user_id`,`is_delete`) VALUES ('2', '" + \
                          post['title'] + ' ' + lastTitle[0] + "', '" + cm[1] + "', '" + str(post['type']) + "','0','" + cm[0] + "', '0', '" + cm[2] + "', '0','0');"
                    print(sql)
                    self.__cursor.execute(sql)
                    self.__conn.commit()
                    # return True
                except mariadb.Error as error:
                    self.__conn.rollback()
                    print("Error: {}".format(error))
                    return False
        else:
            print("add activity but db not connected")
            return False

#     def update_all_restaurant_picture_from_ref(self, quota):
#         # 去資料庫restaurants找餐廳的資料，僅找quota筆資料
#         # 根據欄位google_photo_reference找google圖供的圖片
#         # 下載圖片到電腦
#         # 把電腦路徑存在資料庫裡
#         if self.__check_connect_db():
#             if quota <= 0:
#                 return -1
#             sql = "SELECT * FROM restaurants WHERE picture IS NULL AND google_photo_reference IS NOT NULL LIMIT 1"
#             # sql = "SELECT * FROM restaurants WHERE picture IS NULL AND google_photo_reference IS NOT NULL LIMIT "\
#             #       + str(quota)
#             self.__cursor.execute(sql)
#             data = self.__cursor.fetchall()
#             gp = GooglePic.GooglePic()
#             count = 0
#             try:
#                 for rest in data:
#                     filePath = gp.store_pic_according_to_ref(rest)
#                     updateSql = "UPDATE restaurants SET google_photo_reference=NULL WHERE id=" + str(rest[0]) + ";"
#                     if filePath != 0:
#                         updateSql = "UPDATE restaurants SET picture='" + filePath + "' WHERE id=" + str(rest[0]) + ";"
#                         count = count + 1
#                     self.__cursor.execute(updateSql)
#                     self.__conn.commit()
# #                    if count >= 2:
# #                        break
#             except mariadb.Error as error:
#                 print(error)
#             return count
#         else:
#             print("update_all_restaurant but db not connected")
#             return -2
#
    def truncate_table(self, tableName):
        if self.__check_connect_db():
            sql = "TRUNCATE " + tableName
            self.__cursor.execute(sql)
            self.__conn.commit()

